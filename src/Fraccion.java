
public class Fraccion {
	 /* n y b argumento tipos int 
	 * s para las de tipo String
	 */
	    int numerador;
	    int denominador;
	    
	    // funciones estaticas
		// retormana n que es MCD entre dos numeros <--> a>b

		public static int MCD(int a, int b) {
		    int temporal;//Para no perder b
		    while (b != 0) {
		        temporal = b;
		        b = a % b;
		        a = temporal;
		    }
		    return (a<0)?a*-1:a;
		}
		
	public static boolean esPrimoEnR(int a) {
		if(a>0) {
			for (int i =1 ; i<a ;i++ ) {
				if(a%i == 0) {
					return true;

				}
					}

		}else {
			for (int i =-1 ; i>a ;i-- ) {
				if(a%i == 0) {
					return true;

				}
					}
			
		}
		return false;
	}
		
		
		// fin de funciones estaticas
	    
	//funciones de la clase propia    
	// imprime una  linea con correspondencia a la longitud de String de mayor caracteres  
	    public static String longitud(String s, String s2) {
	    	String linea = "";
	    	for (int i = 0 ; i< Math.max(s.length(), s2.length())  ; i++) {
	    		linea=linea+".";
	    	}
	    	return linea;
	    }
	    // Esta funcion es static porque un metodo no se puede dar vida a si mismo
	    //
	    public static Fraccion dividir(Fraccion f1 , Fraccion f2) {
	       	Fraccion invertir = new Fraccion(f2.denominador,f2.numerador);
	    	return new Fraccion( Fraccion.producto(f1,invertir).numerador ,Fraccion.producto(f1,invertir).denominador );
	    }

	    public static Fraccion producto(Fraccion f1 , Fraccion f2) {
	    	
	    	int n , d;
	    	n = (f1.numerador*f2.numerador);
	    	d = (f1.denominador*f2.denominador);
	    	
	    	return new Fraccion( n/MCD(n,d) , d/MCD(n,d) );
	    }
	    public static Fraccion suma(Fraccion f1 , Fraccion f2) {
	    	
	    	int n , d;
	    	int mcd = MCD(f1.denominador,f2.denominador);
	    	n = (   (f1.denominador/mcd  )* f1.numerador ) +(( f2.denominador/mcd )* f2.numerador);
	    	d = (mcd);
	    	
	    	return new Fraccion( n/MCD(n,d) , d/MCD(n,d) );
	    }
	    public static Fraccion resta(Fraccion f1 , Fraccion f2) {
	    	
	    	int n , d;
	    	int mcd = MCD(f1.denominador,f2.denominador);
	    	n = (   (f1.denominador/mcd  )* f1.numerador ) -(( f2.denominador/mcd )* f2.numerador);
	    	d = (mcd);
	    	
	    	return new Fraccion( n/MCD(n,d) , d/MCD(n,d) );
	    }
	//*--------------
	    //contructor de la clase
	 	public Fraccion(int numerador, int denominador) {
			if (denominador== 0) {
				throw new IllegalArgumentException(" En denominador debe ser != "+denominador);
			}
			this.numerador = numerador;
			this.denominador = denominador;
		}
		
		//-----------------------
		//metodos de la clase...
		public void mostrar() {
			if(numerador == denominador) {
				numerador = 1 ;
				denominador =1 ;
				System.out.println(numerador);
				return;
			}
			System.out.println(numerador);
			System.out.println(longitud(numerador+"",denominador+""));
			System.out.println(denominador);

		}
		public void invertirsigno() {
			
			if (numerador < 0 && denominador > 0) {
				numerador*=-1;

			}else {
				if (numerador > 0 && denominador < 0) {
					denominador*=-1;
				}else {
					if (numerador < 0 && denominador < 0) {
						denominador*=-1;
						
					}else {
						numerador*=-1;
					}
				}
				
				
			}
			

		}
		
		
		public void invertir() {
			if (numerador== 0) {
				throw new IllegalArgumentException(" En denominador debe ser != "+denominador);
			}
			int aux  =denominador;
			this.denominador = numerador;
			this.numerador = aux;	
		}
		public double aDouble() {
			double n1 , n2 ;
			n1 = numerador+0.0;
			n2 = denominador+0.0;

			return n1/n2;
		}
		public void reducir() {
			if (numerador  == denominador) {
				numerador = 1;
				denominador = 1;
			}
			numerador = numerador /MCD(numerador,denominador);
			denominador = denominador /MCD(numerador,denominador);
		}
		
		// prueba de los metodos y funciones fracción
		public static void main(String [] args) {
		}/*		Fraccion f = new Fraccion(15,10);
			Fraccion f2 = new Fraccion(10,10);
		f.invertirsigno();
		f.invertir();
			//f.reducir();
		Fraccion.dividir(f,f2).mostrar();
		f.mostrar();
		int c = MCD(-15, 7);
		System.out.println(c);
		System.out.print(7);

		}
	*/	
	}

